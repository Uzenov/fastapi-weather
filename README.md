### Python3  ##

* FastAPI app 
* PostgreSQL integration using SQLAlchemy
* Dockerfile and docker-compose integations


### How to run this with Docker? ###

* Make sure you have docker installed and runninng on your machine
* Open the terminal to the docker-compose path and hit the following command - 
 ```
 MYCOMPUTER:FastAPI user$ docker-compose up --build
 ```
 
*Dev last audit [22.01.2022] - Ilyas Uzenov*