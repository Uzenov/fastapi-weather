from datetime import date
from pydantic import BaseModel
from typing import Optional, Dict

from sqlalchemy.dialects.postgresql import json


__author__ = 'Uzenov'


class WeatherInfo(BaseModel):
    id: int
    country_code: str
    city: str
    date: Optional[date]
    data: Dict[str, float] = None

    class Config:
        orm_mode = True

