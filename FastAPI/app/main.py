from datetime import datetime

import requests
from fastapi import FastAPI, Depends, HTTPException
from .database import SessionLocal, engine
from sqlalchemy.orm import Session
from .schema import WeatherInfo
from . import crud, models


__author__ = 'Uzenov'


models.Base.metadata.create_all(bind=engine)

app = FastAPI()

API_key = '4829611cffacac7d658507c4c9596bc6'
# base_url = "http://api.openweathermap.org/data/2.5/weather?"
base_url = "http://api.openweathermap.org/data/2.5/forecast?"

def db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@app.get('/weather/info/')
def get_device_info(city: str, country_code:str, date:datetime, db=Depends(db)):
    info = crud.get_weather_info(db,city, date, country_code)
    if info:
        return info
    else:
        try:
            r = requests.get(
                f"{base_url}q={city},{country_code}&appid={API_key}&units=metric"
            )
            weather_data = r.json()

            date_format = datetime.datetime.strptime(date + ' 12:00',
                                         "%m/%d/%Y, %H:%M:%S")
            unix_time = datetime.datetime.timestamp(date_format)
            for forecast in weather_data["list"]:
                if forecast.dt == unix_time:
                     info.data =  forecast
                    break

            crud.save_weather_info(db, info)

            return weather_data
        except Exception as e:
            return {"error": "Exception (weather)", "message": e}



