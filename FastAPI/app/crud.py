from datetime import datetime

from sqlalchemy.orm import Session
from . import schema, models


__author__ = 'Uzenov'


def save_weather_info(db: Session, info: schema.WeatherInfo):
    weather_info_model = models.WeatherInfo(**info.dict())
    db.add(weather_info_model)
    db.commit()
    db.refresh(weather_info_model)
    return weather_info_model


def get_weather_info(db: Session, city: str = None, date: datetime = None, country_code: str = None, data: dict=None):
    if city is None:
        return db.query(models.WeatherInfo).all()
    else:
        return db.query(models.WeatherInfo).filter(models.WeatherInfo.city == city,
                                                   models.WeatherInfo.country_code == country_code,
                                                   models.WeatherInfo.date == date
                                                   ).first()


def error_message(message):
    return {
        'error': message
    }