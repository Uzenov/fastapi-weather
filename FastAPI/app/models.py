from .database import Base
from sqlalchemy import Column, String, Boolean, Integer, JSON


__author__ = 'Uzenov'


class WeatherInfo(Base):
    __tablename__ = 'DeviceInfo'
    id = Column(Integer, primary_key=True)
    country_code = Column(String(3), nullable=False)
    city = Column(String(30), nullable=False)
    date = Column(String(30), nullable=False)
    data = Column(JSON)


